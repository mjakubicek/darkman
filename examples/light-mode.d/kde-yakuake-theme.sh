#!/usr/bin/env bash

# Yakuake is a drop-down terminal emulator based on KDE Konsole technology.
# KDE's default terminal emulator supports profiles, you can create one in
# Settings > Manage Profiles. You can select a dark or light theme in
# Appearance > Color scheme and font. The following script iterates over all
# instances of Konsole und changes the profile of all sessions. This is necessary,
# if there are multiple tabs in one of the Konsole instances.
# Reference: https://docs.kde.org/stable5/en/konsole/konsole/konsole.pdf

PROFILE='Breath-light'
qdbus org.kde.yakuake | grep '^/Sessions/' | xargs -I'{}' qdbus org.kde.yakuake '{}' setProfile "$PROFILE"
qdbus org.kde.yakuake | grep '^/Windows/' | xargs -I'{}' qdbus org.kde.yakuake '{}' setDefaultProfile "$PROFILE"
